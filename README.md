### Back-office Brand Demo application

##### How to run MySql DB:
cd brand-stock-app/config
Start mysql db:
```
cd docker-compose -f docker-compose.yml up -d
```
Stop mysql db:
```
docker-compose -f docker-compose.yml down
```

##### How to run app:

```
mvn clean install
cd target
java -jar brandEntity-stock-app-1.0-SNAPSHOT.jar --spring.profiles.active=mysql
```

##### How to load DB damp:

If you want to load DB damp automatically during the start of app,
make such changes in application.yml:

```
spring:
  batch:
    job:
      enabled: true
```

Otherwise you can trigger batch load brands API endpoint:
GET: http://localhost:8080/brands/load

#### How to get generated swagger documentation:
Link to swagger UI: http://localhost:8080/swagger-ui.html#/