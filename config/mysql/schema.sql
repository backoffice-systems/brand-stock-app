CREATE TABLE IF NOT EXISTS brand
(
    brand_id bigint not null PRIMARY KEY auto_increment ,
    name     varchar(255)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS quantity
(
    id            bigint not null,
    quantity      integer,
    time_received TIMESTAMP,
    brand_id      bigint,
    primary key (id),
    KEY brand_id__fk (brand_id),
    CONSTRAINT brand_id__fk FOREIGN KEY (brand_id) REFERENCES brand (brand_id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS quantity_sequence
(
    next_val bigint
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO quantity_sequence (next_val) values (1);
