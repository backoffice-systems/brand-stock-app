package com.backoffice.brands.repository;

import com.backoffice.brands.entity.QuantityEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuantityRepository extends JpaRepository<QuantityEntity, Long> {}
