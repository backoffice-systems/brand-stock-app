package com.backoffice.brands.repository;

import com.backoffice.brands.api.model.BrandRow;
import com.backoffice.brands.entity.BrandEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface BrandRepository
    extends JpaRepository<BrandEntity, Long>, PagingAndSortingRepository<BrandEntity, Long> {

  Optional<BrandEntity> findByName(final String name);

  @Query(
      "SELECT new com.backoffice.brands.api.model.BrandRow(b.id, b.name, SUM(q.quantity)) "
          + " FROM BrandEntity b "
          + " LEFT JOIN b.quantities q "
          + " GROUP BY b.id")
  Page<BrandRow> generateSumReport(final Pageable pageable);
}
