package com.backoffice.brands.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.backoffice.brands.constants.Constants.BRAND;
import static com.backoffice.brands.constants.Constants.ID;
import static com.backoffice.brands.constants.Constants.NAME;
import static com.backoffice.brands.constants.Constants.QUANTITIES;
import static com.backoffice.brands.constants.Constants.QUANTITY;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = BRAND)
@XmlAccessorType(XmlAccessType.FIELD)
public class Brand implements Serializable {
  private static final long serialVersionUID = 2L;

  @JsonProperty(ID)
  @XmlElement(name = ID)
  private Long id;

  @JsonProperty(NAME)
  @XmlElement(name = NAME)
  private String name;

  @JsonProperty(QUANTITIES)
  @XmlElementWrapper(name = QUANTITIES)
  @XmlElement(name = QUANTITY)
  private List<Quantity> quantities = new ArrayList<>();
}
