package com.backoffice.brands.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

import static com.backoffice.brands.constants.Constants.ERROR;
import static com.backoffice.brands.constants.Constants.MESSAGE;

@Data
@AllArgsConstructor
@XmlRootElement(name = ERROR)
@XmlAccessorType(XmlAccessType.FIELD)
public class ErrorResponse implements Serializable {
  private static final long serialVersionUID = 1L;

  @JsonProperty(MESSAGE)
  @XmlElement(name = MESSAGE)
  private String message;
}
