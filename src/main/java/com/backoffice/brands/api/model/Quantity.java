package com.backoffice.brands.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.time.Instant;

import static com.backoffice.brands.constants.Constants.ID;
import static com.backoffice.brands.constants.Constants.QUANTITY;
import static com.backoffice.brands.constants.Constants.RECEIVED_TIME;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = QUANTITY)
@XmlAccessorType(XmlAccessType.FIELD)
public class Quantity implements Serializable {
  private static final long serialVersionUID = 3L;

  @JsonProperty(ID)
  @XmlElement(name = ID)
  private Long id;

  @JsonProperty(RECEIVED_TIME)
  @XmlElement(name = RECEIVED_TIME)
  private Instant receivedTime;

  @JsonProperty(QUANTITY)
  @XmlElement(name = QUANTITY)
  private Integer quantity;
}
