package com.backoffice.brands.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import static com.backoffice.brands.constants.Constants.ID;
import static com.backoffice.brands.constants.Constants.NAME;
import static com.backoffice.brands.constants.Constants.QUANTITY;
import static com.backoffice.brands.constants.Constants.BRAND;

@Data
@Builder
@NoArgsConstructor
@XmlRootElement(name = BRAND)
@XmlAccessorType(XmlAccessType.FIELD)
public class BrandRow {

  @JsonProperty(ID)
  @XmlElement(name = ID)
  private Long id;

  @JsonProperty(NAME)
  @XmlElement(name = NAME)
  private String name;

  @JsonProperty(QUANTITY)
  @XmlElement(name = QUANTITY)
  private Long quantity = 0L;

  public BrandRow(Long id, String name, Long quantity) {
    this.id = id;
    this.name = name;
    this.quantity = quantity == null ? 0L : quantity;
  }
}
