package com.backoffice.brands.api.controller;

import com.backoffice.brands.api.model.ErrorResponse;
import com.backoffice.brands.batch.service.BatchLoadService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping(value = "/brands/load")
@Api(value = "batch-jobs")
public class BrandLoadController {
  @Autowired private BatchLoadService brandBatchLoadService;

  @ApiOperation(
      value = "load-batch-job",
      nickname = "load-batch-job",
      tags = {"brand-load-controller"})
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 500, message = "Internal server error", response = ErrorResponse.class)
      })
  @GetMapping
  public ResponseEntity<?> load() throws Exception {
    return ResponseEntity.ok(
        new HashMap<String, String>() {
          {
            put("status", brandBatchLoadService.run().name());
          }
        });
  }
}
