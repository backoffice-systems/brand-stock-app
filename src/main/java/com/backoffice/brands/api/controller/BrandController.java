package com.backoffice.brands.api.controller;

import com.backoffice.brands.api.model.Brand;
import com.backoffice.brands.api.model.BrandRow;
import com.backoffice.brands.api.model.ErrorResponse;
import com.backoffice.brands.service.BrandService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

@RestController
@RequestMapping("/brands")
@Api(value = "brands")
public class BrandController {

  @Autowired private BrandService brandService;

  @ApiOperation(
      value = "createBrand",
      nickname = "createBrand",
      response = Brand.class,
      tags = {"brand-controller"})
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "OK", response = Brand.class),
        @ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ErrorResponse.class)
      })
  @RequestMapping(
      produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE},
      consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE},
      method = RequestMethod.POST)
  public ResponseEntity<Brand> createBrand(@RequestBody final Brand brand) {
    return ResponseEntity.ok(brandService.save(brand));
  }

  @ApiOperation(
      value = "getBrand",
      nickname = "getBrand",
      response = Brand.class,
      tags = {"brand-controller"})
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "OK", response = Brand.class),
        @ApiResponse(code = 404, message = "Not Found", response = ErrorResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ErrorResponse.class)
      })
  @RequestMapping(
      value = "/{brandId}",
      produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE},
      consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE},
      method = RequestMethod.GET)
  public ResponseEntity<Brand> getBrand(
      @ApiParam(value = "brandId", required = true) @PathVariable("brandId") final Long brandId) {
    return ResponseEntity.ok(brandService.getBrand(brandId));
  }

  @ApiOperation(
      value = "deleteBrand",
      nickname = "deleteBrand",
      tags = {"brand-controller"})
  @ApiResponses(
      value = {
        @ApiResponse(code = 204, message = "No content"),
        @ApiResponse(code = 404, message = "Not Found", response = ErrorResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ErrorResponse.class)
      })
  @RequestMapping(
      value = "/{brandId}",
      produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE},
      consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE},
      method = RequestMethod.DELETE)
  public ResponseEntity<Brand> deleteBrand(
      @ApiParam(value = "brandId", required = true) @PathVariable("brandId") final Long brandId) {
    brandService.deleteBrand(brandId);
    return ResponseEntity.noContent().build();
  }

  @ApiOperation(
      value = "updateBrand",
      nickname = "updateBrand",
      response = Brand.class,
      tags = {"brand-controller"})
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "Ok", response = Brand.class),
        @ApiResponse(code = 404, message = "Not Found", response = ErrorResponse.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ErrorResponse.class)
      })
  @RequestMapping(
      value = "/{brandId}",
      produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE},
      consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE},
      method = RequestMethod.PUT)
  public ResponseEntity<Brand> updateBrand(
      @ApiParam(value = "brandId", required = true) @PathVariable("brandId") final Long brandId,
      @ApiParam(value = "brand", required = true) @RequestBody final Brand brand) {
    return ResponseEntity.ok(brandService.updateBrand(brandId, brand));
  }

  @ApiOperation(
      value = "generateSumReport",
      nickname = "generateSumReport",
      response = Page.class,
      tags = {"brand-controller"})
  @ApiResponses(
      value = {
        @ApiResponse(code = 200, message = "Ok", response = Page.class),
        @ApiResponse(code = 500, message = "Internal server error", response = ErrorResponse.class)
      })
  @RequestMapping(
      value = "/reports/sum",
      produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE},
      consumes = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE},
      method = RequestMethod.GET)
  public ResponseEntity<Page<BrandRow>> generateSumReport(final Pageable pageable) {
    return ResponseEntity.ok(brandService.generateSumReport(pageable));
  }
}
