package com.backoffice.brands.api.exception;

import com.backoffice.brands.api.model.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
  private static final Logger LOGGER =
      LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

  @ExceptionHandler(DuplicateEntityException.class)
  public ResponseEntity<ErrorResponse> handleDuplicateBrandException(DuplicateEntityException e) {
    LOGGER.error(
        "Exception mapper caught DuplicateEntityException {}. Mapping to Bad Request Client Error response",
        e.getMessage());
    return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<ErrorResponse> handleEntityNotFoundException(EntityNotFoundException e) {
    LOGGER.error(
        "Exception mapper caught EntityNotFoundException {}. Mapping to Not Found Request Client Error response",
        e.getMessage());
    return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(value = {Exception.class})
  public ResponseEntity<Object> handleException(final Exception e) {
    LOGGER.error(
        "Exception mapper caught unhandled type of exception. Mapping to generic Server Error response",
        e);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .body(new ErrorResponse("Unknown error"));
  }
}
