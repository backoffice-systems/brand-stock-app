package com.backoffice.brands.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "quantity")
public class QuantityEntity {

  @Id
  @Column(name = "id", updatable = false, nullable = false)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "quantity_seq_gen")
  @SequenceGenerator(
      name = "quantity_seq_gen",
      initialValue = 1,
      sequenceName = "quantity_sequence")
  private Long id;

  @Column(name = "time_received", columnDefinition = "TIMESTAMP")
  private Instant timeReceived;

  @Column(name = "quantity")
  private Integer quantity;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "brand_id", referencedColumnName = "brand_id")
  private BrandEntity brand;
}
