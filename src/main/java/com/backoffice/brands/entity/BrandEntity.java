package com.backoffice.brands.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static com.backoffice.brands.constants.Constants.ID_GENERATOR;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "brand")
public class BrandEntity {

  @Id
  @GeneratedValue(generator = ID_GENERATOR)
  @GenericGenerator(
      name = ID_GENERATOR,
      strategy = "com.backoffice.brands.generator.FilterIdentifierGenerator")
  @Column(name = "brand_id", unique = true, nullable = false)
  private Long brandId;

  @Column(name = "name")
  private String name;

  @OneToMany(
      mappedBy = "brand",
      fetch = FetchType.LAZY,
      cascade = {CascadeType.REFRESH})
  private List<QuantityEntity> quantities = new ArrayList<>();
}
