package com.backoffice.brands.config;

import com.backoffice.brands.batch.*;
import com.backoffice.brands.batch.model.BrandDto;
import com.backoffice.brands.batch.model.QuantityDto;
import com.backoffice.brands.batch.processor.BrandProcessor;
import com.backoffice.brands.batch.processor.QuantityProcessor;
import com.backoffice.brands.batch.writer.BrandWriter;
import com.backoffice.brands.batch.writer.QuantityWriter;
import com.backoffice.brands.entity.BrandEntity;
import com.backoffice.brands.entity.QuantityEntity;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.*;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import static com.backoffice.brands.constants.Constants.BRAND_LOAD_JOB;
import static com.backoffice.brands.constants.Constants.BRAND_LOAD_STEP;
import static com.backoffice.brands.constants.Constants.BRAND_READER_NAME;
import static com.backoffice.brands.constants.Constants.QUANTITY_FILE_INPUT;
import static com.backoffice.brands.constants.Constants.QUANTITY_LOAD_STEP;
import static com.backoffice.brands.constants.Constants.QUANTITY_READER_NAME;
import static java.util.Optional.ofNullable;

@Configuration
@EnableBatchProcessing
public class BrandLoadJobConfiguration {

  @Autowired private JobBuilderFactory jobBuilderFactory;

  @Autowired private StepBuilderFactory stepBuilderFactory;

  @Bean
  public Job job(final Step brandLoad, final Step quantityLoad) {
    return jobBuilderFactory
        .get(BRAND_LOAD_JOB)
        .incrementer(new RunIdIncrementer())
        // .listener(protocolListener())
        .start(brandLoad)
        .next(quantityLoad)
        .build();
  }

  @Bean
  public Step brandLoad(final FlatFileItemReader<BrandDto> brandReader) {
    return stepBuilderFactory
        .get(BRAND_LOAD_STEP)
        .<BrandDto, BrandEntity>chunk(1)
        .reader(brandReader)
        .processor(brandProcessor())
        .writer(brandWriter())
        .build();
  }

  @Bean
  public Step quantityLoad(final FlatFileItemReader<QuantityDto> quantityReader) {
    return stepBuilderFactory
        .get(QUANTITY_LOAD_STEP)
        .<QuantityDto, QuantityEntity>chunk(1)
        .reader(quantityReader)
        .processor(quantityProcessor())
        .writer(quantityWriter())
        .build();
  }

  @StepScope
  @Bean
  public FlatFileItemReader<BrandDto> brandReader(
      @Value("#{jobParameters['brand.file.input']}") String path,
      @Value("${file.input.brand:#{null}}") String optionalPath) {
    final DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
    lineTokenizer.setDelimiter("\t");
    lineTokenizer.setStrict(false);

    final BeanWrapperFieldSetMapper<BrandDto> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
    fieldSetMapper.setTargetType(BrandDto.class);

    final DefaultLineMapper<BrandDto> lineMapper = new DefaultLineMapper<>();
    lineMapper.setLineTokenizer(lineTokenizer);
    lineMapper.setFieldSetMapper(fieldSetMapper);

    final FlatFileItemReader<BrandDto> reader = new FlatFileItemReader<>();
    reader.setName(BRAND_READER_NAME);
    reader.setLinesToSkip(1);
    reader.setSkippedLinesCallback(line -> lineTokenizer.setNames(line.split("\t")));
    reader.setResource(new ClassPathResource(ofNullable(path).orElse(optionalPath)));
    reader.setLineMapper(lineMapper);
    reader.setRecordSeparatorPolicy(new BlankLineRecordSeparatorPolicy());
    return reader;
  }

  @Bean
  public ItemProcessor<BrandDto, BrandEntity> brandProcessor() {
    return new BrandProcessor();
  }

  @Bean
  public ItemWriter<BrandEntity> brandWriter() {
    return new BrandWriter();
  }

  @StepScope
  @Bean
  public FlatFileItemReader<QuantityDto> quantityReader(
      @Value("#{jobParameters['" + QUANTITY_FILE_INPUT + "']}") String path,
      @Value("${file.input.quantity:#{null}}") String optionalPath) {
    final DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
    lineTokenizer.setDelimiter("\t");
    lineTokenizer.setStrict(false);

    final BeanWrapperFieldSetMapper<QuantityDto> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
    fieldSetMapper.setTargetType(QuantityDto.class);

    final DefaultLineMapper<QuantityDto> lineMapper = new DefaultLineMapper<>();
    lineMapper.setLineTokenizer(lineTokenizer);
    lineMapper.setFieldSetMapper(fieldSetMapper);

    final FlatFileItemReader<QuantityDto> reader = new FlatFileItemReader<>();
    reader.setName(QUANTITY_READER_NAME);
    reader.setLinesToSkip(1);
    reader.setSkippedLinesCallback(line -> lineTokenizer.setNames(line.split("\t")));
    reader.setResource(new ClassPathResource(ofNullable(path).orElse(optionalPath)));
    reader.setLineMapper(lineMapper);
    reader.setRecordSeparatorPolicy(new BlankLineRecordSeparatorPolicy());
    return reader;
  }

  @Bean
  public ItemProcessor<QuantityDto, QuantityEntity> quantityProcessor() {
    return new QuantityProcessor();
  }

  @Bean
  public ItemWriter<QuantityEntity> quantityWriter() {
    return new QuantityWriter();
  }
}
