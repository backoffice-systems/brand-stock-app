package com.backoffice.brands.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {
  @Bean
  public Docket api(@Autowired ApiInfo apiInfo) {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.backoffice.brands"))
        .paths(PathSelectors.regex("/.*"))
        .build()
        .useDefaultResponseMessages(false)
        .apiInfo(apiInfo);
  }

  @Bean
  public ApiInfo apiEndpointsInfo() {
    return new ApiInfoBuilder()
        .title("Back-office Systems Brands REST API")
        .description("Brands REST API")
        .version("1.0.0")
        .build();
  }
}
