package com.backoffice.brands.generator;

import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.id.IdentityGenerator;

import java.io.Serializable;

public class FilterIdentifierGenerator extends IdentityGenerator implements IdentifierGenerator {
  @Override
  public Serializable generate(final SharedSessionContractImplementor session, final Object obj) {
    Serializable id =
        session.getEntityPersister(null, obj).getClassMetadata().getIdentifier(obj, session);
    return id != null ? id : super.generate(session, obj);
  }
}
