package com.backoffice.brands.constants;

public interface Constants {

  // Job related constants
  String BRAND_LOAD_JOB = "brandLoadJob";
  String BRAND_LOAD_STEP = "brandLoadStep";
  String QUANTITY_LOAD_STEP = "quantityLoadStep";
  String BRAND_READER_NAME = "brandReader";
  String QUANTITY_READER_NAME = "quantityReader";
  String QUANTITY_FILE_INPUT = "quantity.file.input";
  String BRAND_FILE_INPUT = "brand.file.input";
  String TIME_PARAMETER = "time";

  // ORM constants
  String ID_GENERATOR = "brandIdsGenerator";

  // JSON/XML fields mapping
  String ID = "id";
  String NAME = "name";
  String QUANTITY = "quantity";
  String QUANTITIES = "quantities";
  String BRAND = "brand";
  String MESSAGE = "message";
  String ERROR = "error";
  String RECEIVED_TIME = "receivedTime";
}
