package com.backoffice.brands.batch.service;

import org.springframework.batch.core.BatchStatus;

public interface BatchLoadService {

  BatchStatus run() throws Exception;
}
