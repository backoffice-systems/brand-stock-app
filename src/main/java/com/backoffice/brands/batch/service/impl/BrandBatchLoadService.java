package com.backoffice.brands.batch.service.impl;

import com.backoffice.brands.batch.service.BatchLoadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static com.backoffice.brands.constants.Constants.BRAND_FILE_INPUT;
import static com.backoffice.brands.constants.Constants.QUANTITY_FILE_INPUT;
import static com.backoffice.brands.constants.Constants.TIME_PARAMETER;

@Service
public class BrandBatchLoadService implements BatchLoadService {
  private static final Logger LOGGER = LoggerFactory.getLogger(BrandBatchLoadService.class);

  @Autowired private JobLauncher jobLauncher;
  @Autowired private Job job;

  @Value("${file.input.brand}")
  private String brandFileName;

  @Value("${file.input.quantity}")
  private String quantityFileName;

  @Override
  public BatchStatus run() throws Exception {
    final JobExecution jobExecution = jobLauncher.run(job, getJobParameters());
    LOGGER.info("JobExecution status: {}", jobExecution.getStatus());
    while (jobExecution.isRunning()) {
      LOGGER.info("Batch is running... ");
    }
    return jobExecution.getStatus();
  }

  private JobParameters getJobParameters() {
    final Map<String, JobParameter> parameters = new HashMap<>();
    parameters.put(BRAND_FILE_INPUT, new JobParameter(brandFileName));
    parameters.put(QUANTITY_FILE_INPUT, new JobParameter(quantityFileName));
    parameters.put(TIME_PARAMETER, new JobParameter(System.currentTimeMillis()));

    return new JobParameters(parameters);
  }
}
