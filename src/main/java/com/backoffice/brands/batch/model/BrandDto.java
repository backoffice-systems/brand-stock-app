package com.backoffice.brands.batch.model;

import lombok.Data;

@Data
public class BrandDto {

  private Long brandId;
  private String name;
}
