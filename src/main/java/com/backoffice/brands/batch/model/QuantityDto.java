package com.backoffice.brands.batch.model;

import lombok.Data;

@Data
public class QuantityDto {

  private String timeReceived;
  private Integer quantity;
  private Long brandId;
}
