package com.backoffice.brands.batch.writer;

import com.backoffice.brands.entity.BrandEntity;
import com.backoffice.brands.service.BrandService;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class BrandWriter implements ItemWriter<BrandEntity> {

  @Autowired private BrandService brandService;

  @Override
  public void write(List<? extends BrandEntity> brands) {
    brandService.saveAll((List<BrandEntity>) brands);
  }
}
