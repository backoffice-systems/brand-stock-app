package com.backoffice.brands.batch.writer;

import com.backoffice.brands.entity.QuantityEntity;
import com.backoffice.brands.service.QuantityService;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class QuantityWriter implements ItemWriter<QuantityEntity> {

  @Autowired private QuantityService quantityService;

  @Override
  public void write(List<? extends QuantityEntity> items) {
    quantityService.saveAll((List<QuantityEntity>) items);
  }
}
