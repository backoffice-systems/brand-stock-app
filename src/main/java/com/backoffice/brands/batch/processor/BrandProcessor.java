package com.backoffice.brands.batch.processor;

import com.backoffice.brands.batch.model.BrandDto;
import com.backoffice.brands.entity.BrandEntity;
import org.springframework.batch.item.ItemProcessor;

public class BrandProcessor implements ItemProcessor<BrandDto, BrandEntity> {

  @Override
  public BrandEntity process(BrandDto brandDto) {
    return BrandEntity.builder().brandId(brandDto.getBrandId()).name(brandDto.getName()).build();
  }
}
