package com.backoffice.brands.batch.processor;

import com.backoffice.brands.batch.model.QuantityDto;
import com.backoffice.brands.entity.BrandEntity;
import com.backoffice.brands.entity.QuantityEntity;
import com.backoffice.brands.service.BrandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.Optional;

public class QuantityProcessor implements ItemProcessor<QuantityDto, QuantityEntity> {

  private static final Logger LOGGER = LoggerFactory.getLogger(QuantityProcessor.class);

  @Autowired private BrandService brandService;

  @Override
  public QuantityEntity process(final QuantityDto quantityDto) {
    Optional<BrandEntity> foundBrand = brandService.findBrandById(quantityDto.getBrandId());
    if (foundBrand.isPresent()) {
      return QuantityEntity.builder()
          .timeReceived(Instant.parse(quantityDto.getTimeReceived()))
          .brand(foundBrand.get())
          .quantity(quantityDto.getQuantity())
          .build();
    }
    LOGGER.info(
        "BrandEntity with id: {} was not found, skip quantity row {}",
        quantityDto.getBrandId(),
        quantityDto);
    return null;
  }
}
