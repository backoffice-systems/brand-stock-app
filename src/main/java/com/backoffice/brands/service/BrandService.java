package com.backoffice.brands.service;

import com.backoffice.brands.api.model.Brand;
import com.backoffice.brands.api.model.BrandRow;
import com.backoffice.brands.entity.BrandEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface BrandService {
  BrandEntity save(final BrandEntity brandEntity);

  Brand save(final Brand brand);

  List<BrandEntity> saveAll(final List<BrandEntity> brands);

  Optional<BrandEntity> findBrandById(final Long brandId);

  Optional<BrandEntity> findBrandByName(final String name);

  Brand getBrand(final Long id);

  void deleteBrand(final Long id);

  Brand updateBrand(final Long id, final Brand brand);

  Page<BrandRow> generateSumReport(final Pageable pageable);
}
