package com.backoffice.brands.service.impl;

import com.backoffice.brands.entity.QuantityEntity;
import com.backoffice.brands.repository.QuantityRepository;
import com.backoffice.brands.service.QuantityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Transactional
@Service
public class QuantityServiceImpl implements QuantityService {
  @Autowired private QuantityRepository quantityRepository;

  @Override
  public QuantityEntity save(final QuantityEntity quantityEntity) {
    return quantityRepository.saveAndFlush(quantityEntity);
  }

  @Override
  public List<QuantityEntity> saveAll(final List<QuantityEntity> items) {
    if (CollectionUtils.isEmpty(items)) {
      return items;
    }
    return quantityRepository.saveAll(items);
  }

  @Override
  public void deleteAll(List<QuantityEntity> items) {
    if (!CollectionUtils.isEmpty(items)) {
      quantityRepository.deleteAll(items);
    }
  }
}
