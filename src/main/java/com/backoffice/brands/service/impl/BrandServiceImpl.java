package com.backoffice.brands.service.impl;

import com.backoffice.brands.api.exception.DuplicateEntityException;
import com.backoffice.brands.api.exception.EntityNotFoundException;
import com.backoffice.brands.api.model.Brand;
import com.backoffice.brands.api.model.BrandRow;
import com.backoffice.brands.entity.BrandEntity;
import com.backoffice.brands.repository.BrandRepository;
import com.backoffice.brands.service.BrandService;
import com.backoffice.brands.service.QuantityService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class BrandServiceImpl implements BrandService {
  @Autowired private BrandRepository brandRepository;
  @Autowired private QuantityService quantityService;
  @Autowired private ModelMapper modelMapper;

  @Override
  public BrandEntity save(final BrandEntity brandEntity) {
    if (brandEntity == null) {
      return null;
    }
    final Optional<BrandEntity> foundBrand = findBrandById(brandEntity.getBrandId());
    if (foundBrand.isPresent()) {
      foundBrand.get().setName(brandEntity.getName());
      return brandRepository.saveAndFlush(foundBrand.get());
    }
    return brandRepository.saveAndFlush(brandEntity);
  }

  @Override
  public Brand save(final Brand brand) {
    if (brand == null) {
      return null;
    }
    final Optional<BrandEntity> foundBrand = findBrandByName(brand.getName());
    if (foundBrand.isPresent()) {
      throw new DuplicateEntityException(
          "Brand with name: " + brand.getName() + " has already exist");
    }
    final BrandEntity brandEntity =
        brandRepository.saveAndFlush(BrandEntity.builder().name(brand.getName()).build());
    return modelMapper.map(brandEntity, Brand.class);
  }

  @Override
  public List<BrandEntity> saveAll(final List<BrandEntity> brands) {
    if (CollectionUtils.isEmpty(brands)) {
      return brands;
    }
    return brandRepository.saveAll(brands);
  }

  @Override
  public Optional<BrandEntity> findBrandById(final Long brandId) {
    if (brandId == null) {
      return Optional.empty();
    }
    return brandRepository.findById(brandId);
  }

  @Override
  public Optional<BrandEntity> findBrandByName(final String name) {
    return brandRepository.findByName(name);
  }

  @Override
  public Brand getBrand(final Long id) {
    final Optional<BrandEntity> foundBrand = findBrandById(id);
    if (foundBrand.isPresent()) {
      return modelMapper.map(foundBrand.get(), Brand.class);
    }
    throw new EntityNotFoundException("Brand with id=" + id + " was not found");
  }

  @Override
  public void deleteBrand(final Long id) {
    final Optional<BrandEntity> foundBrand = findBrandById(id);
    if (foundBrand.isPresent()) {
      final BrandEntity brandEntity = foundBrand.get();
      quantityService.deleteAll(brandEntity.getQuantities());
      brandRepository.delete(brandEntity);
    } else {
      throw new EntityNotFoundException("Brand with id=" + id + " was not found");
    }
  }

  @Override
  public Brand updateBrand(final Long id, final Brand brand) {
    final Optional<BrandEntity> foundBrand = findBrandById(id);
    if (foundBrand.isPresent()) {
      foundBrand.get().setName(brand.getName());
      final BrandEntity modifiedBrand = brandRepository.save(foundBrand.get());
      return modelMapper.map(modifiedBrand, Brand.class);
    }
    throw new EntityNotFoundException("Brand with id=" + id + " was not found");
  }

  @Override
  public Page<BrandRow> generateSumReport(final Pageable pageable) {
    return brandRepository.generateSumReport(pageable);
  }
}
