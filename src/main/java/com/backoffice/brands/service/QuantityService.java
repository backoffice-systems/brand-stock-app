package com.backoffice.brands.service;

import com.backoffice.brands.entity.QuantityEntity;

import java.util.List;

public interface QuantityService {

  QuantityEntity save(QuantityEntity quantityEntity);

  List<QuantityEntity> saveAll(List<QuantityEntity> items);

  void deleteAll(List<QuantityEntity> items);

}
