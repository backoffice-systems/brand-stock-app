package com.backoffice.brands.service;

import com.backoffice.brands.api.exception.DuplicateEntityException;
import com.backoffice.brands.api.exception.EntityNotFoundException;
import com.backoffice.brands.api.model.Brand;
import com.backoffice.brands.entity.BrandEntity;
import com.backoffice.brands.repository.BrandRepository;
import com.backoffice.brands.service.impl.BrandServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BrandServiceImplTest {
  @Mock private BrandRepository brandRepository;
  @Mock private ModelMapper modelMapper;
  @Mock private QuantityService quantityService;
  @InjectMocks private BrandServiceImpl brandService;

  @Test
  public void shouldAddNewBrand() {
    final Brand brand = Brand.builder().name("Test A").build();

    when(brandRepository.findByName("Test A")).thenReturn(Optional.empty());

    brandService.save(brand);

    verify(brandRepository, times(1)).saveAndFlush(any(BrandEntity.class));
  }

  @Test
  public void shouldThrowDuplicateEntityExceptionIfBrandWithNameExists() {
    final Brand brand = Brand.builder().name("Test A").build();
    final BrandEntity brandEntity = BrandEntity.builder().brandId(1L).name("Test A").build();

    when(brandRepository.findByName("Test A")).thenReturn(Optional.of(brandEntity));

    Assertions.assertThrows(DuplicateEntityException.class, () -> brandService.save(brand));
  }

  @Test
  public void shouldModifyExistentBrandEntity() {
    final BrandEntity newEntity = BrandEntity.builder().brandId(1L).name("Test B").build();
    final BrandEntity oldEntity = BrandEntity.builder().brandId(1L).name("Test A").build();

    when(brandRepository.findById(1L)).thenReturn(Optional.of(oldEntity));

    brandService.save(newEntity);

    verify(brandRepository, times(1)).saveAndFlush(oldEntity);
    Assert.assertEquals(newEntity.getName(), oldEntity.getName());
  }

  @Test
  public void shouldAddBrandEntity() {
    final BrandEntity newEntity = BrandEntity.builder().brandId(1L).name("Test B").build();

    when(brandRepository.findById(1L)).thenReturn(Optional.empty());

    brandService.save(newEntity);

    verify(brandRepository, times(1)).saveAndFlush(newEntity);
  }

  @Test
  public void shouldGetBrandById() {
    final BrandEntity entity = BrandEntity.builder().brandId(1L).name("Test A").build();

    when(brandRepository.findById(1L)).thenReturn(Optional.of(entity));

    brandService.getBrand(1L);

    verify(brandRepository, times(1)).findById(1L);
  }

  @Test
  public void shouldThrowEntityNotFoundExceptionIfBrandWithIdNotExists() {
    when(brandRepository.findById(1L)).thenReturn(Optional.empty());

    Assertions.assertThrows(EntityNotFoundException.class, () -> brandService.getBrand(1L));
  }

  @Test
  public void shouldDeleteBrandById() {
    final BrandEntity entity = BrandEntity.builder().brandId(1L).name("Test A").build();

    when(brandRepository.findById(1L)).thenReturn(Optional.of(entity));

    brandService.deleteBrand(1L);

    verify(brandRepository, times(1)).delete(entity);
  }

  @Test
  public void shouldThrowEntityNotFoundExceptionIfDeletedBrandWithIdNotExists() {
    when(brandRepository.findById(1L)).thenReturn(Optional.empty());

    Assertions.assertThrows(EntityNotFoundException.class, () -> brandService.deleteBrand(1L));
  }

  @Test
  public void shouldUpdateBrand() {
    final Brand newBrand = Brand.builder().id(1L).name("Test B").build();
    final BrandEntity oldEntity = BrandEntity.builder().brandId(1L).name("Test A").build();

    when(brandRepository.findById(1L)).thenReturn(Optional.of(oldEntity));

    brandService.updateBrand(1L, newBrand);

    verify(brandRepository, times(1)).save(oldEntity);
    Assert.assertEquals(newBrand.getName(), oldEntity.getName());
  }

  @Test
  public void shouldThrowEntityNotFoundExceptionIfUpdatedBrandWithIdNotExists() {
    final Brand newBrand = Brand.builder().id(1L).name("Test B").build();

    when(brandRepository.findById(1L)).thenReturn(Optional.empty());

    Assertions.assertThrows(
        EntityNotFoundException.class, () -> brandService.updateBrand(1L, newBrand));
  }
}
