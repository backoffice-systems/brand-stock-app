package com.backoffice.brands.batch;

import com.backoffice.brands.config.BrandLoadJobConfiguration;
import com.backoffice.brands.config.GeneralActiveProfileConfig;
import com.backoffice.brands.entity.BrandEntity;
import com.backoffice.brands.service.BrandService;
import com.backoffice.brands.service.QuantityService;
import org.assertj.core.util.Lists;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.*;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.JobRepositoryTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.backoffice.brands.constants.Constants.BRAND_FILE_INPUT;
import static com.backoffice.brands.constants.Constants.QUANTITY_FILE_INPUT;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBatchTest
@EnableAutoConfiguration
@ContextConfiguration(
    classes = {BrandLoadJobConfiguration.class})
public class BrandBatchLoadJobTest implements GeneralActiveProfileConfig {

  @Autowired private JobLauncherTestUtils jobLauncherTestUtils;

  @Autowired private JobRepositoryTestUtils jobRepositoryTestUtils;

  @MockBean private BrandService brandService;

  @MockBean private QuantityService quantityService;

  private JobParameters jobParameters;

  @Before
  public void setup() {
    Map<String, JobParameter> maps = new HashMap<>();
    maps.put(BRAND_FILE_INPUT, new JobParameter("test-brands.tsv"));
    maps.put(QUANTITY_FILE_INPUT, new JobParameter("test-quantity.tsv"));
    maps.put("time", new JobParameter(System.currentTimeMillis()));
    jobParameters = new JobParameters(maps);
  }

  @After
  public void clearJobExecutions() {
    this.jobRepositoryTestUtils.removeJobExecutions();
  }

  @Test
  public void testBrandLoadJob() throws Exception {

    when(brandService.findBrandById(1L))
        .thenReturn(Optional.of(BrandEntity.builder().brandId(1L).name("Test A").build()));
    when(brandService.findBrandById(2L))
        .thenReturn(Optional.of(BrandEntity.builder().brandId(2L).name("Test B").build()));
    when(brandService.findBrandById(25L)).thenReturn(Optional.empty());

    JobExecution jobExecution = this.jobLauncherTestUtils.launchJob(jobParameters);
    List<StepExecution> stepExecutions = Lists.newArrayList(jobExecution.getStepExecutions());
    StepExecution brandStepExecution = stepExecutions.get(0);
    StepExecution quantityStepExecution = stepExecutions.get(1);

    Assert.assertEquals(ExitStatus.COMPLETED, jobExecution.getExitStatus());
    Assert.assertEquals(0, jobExecution.getAllFailureExceptions().size());
    Assert.assertEquals(2, jobExecution.getStepExecutions().size());
    Assert.assertEquals(3, brandStepExecution.getReadCount());
    Assert.assertEquals(3, brandStepExecution.getWriteCount());
    Assert.assertEquals(3, quantityStepExecution.getReadCount());
    Assert.assertEquals(2, quantityStepExecution.getWriteCount());
  }
}
