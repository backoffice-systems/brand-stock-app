package com.backoffice.brands.config;

import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles({"test"})
public interface GeneralActiveProfileConfig {
}
