package com.backoffice.brands.api.controller;

import com.backoffice.brands.BrandsApplication;
import com.backoffice.brands.config.GeneralActiveProfileConfig;
import org.hamcrest.core.Is;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
    scripts = {"classpath:/cleanup.sql"})
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = {BrandsApplication.class})
@ContextConfiguration(initializers = ConfigFileApplicationContextInitializer.class)
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class BrandLoadControlledTest implements GeneralActiveProfileConfig {
  @Autowired private MockMvc mockMvc;

  @Test
  public void shouldReturnStatusOfTheJob() throws Exception {
    final MockHttpServletRequestBuilder requestBuilder =
        MockMvcRequestBuilders.get("/brands/load")
            .accept(MediaType.APPLICATION_JSON_VALUE)
            .contentType(MediaType.APPLICATION_JSON_VALUE);
    ResultActions resultActions = mockMvc.perform(requestBuilder);
    resultActions
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("@.status", Is.is("COMPLETED")));
  }
}
