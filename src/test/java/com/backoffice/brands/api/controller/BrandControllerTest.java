package com.backoffice.brands.api.controller;

import com.backoffice.brands.BrandsApplication;
import com.backoffice.brands.api.model.Brand;
import com.backoffice.brands.config.GeneralActiveProfileConfig;
import com.backoffice.brands.repository.QuantityRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@Sql(
    executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
    scripts = {"classpath:/cleanup.sql", "classpath:/initial_data.sql"})
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = {BrandsApplication.class})
@ContextConfiguration(initializers = ConfigFileApplicationContextInitializer.class)
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class BrandControllerTest implements GeneralActiveProfileConfig {
  @Autowired private MockMvc mockMvc;
  @Autowired private ObjectMapper objectMapper;
  @Autowired private QuantityRepository quantityRepository;

  @Test
  public void shouldCreateNewBrandAndReturn200HttpStatus() throws Exception {
    final Brand request = Brand.builder().name("brand A").build();
    final MockHttpServletRequestBuilder requestBuilder =
        MockMvcRequestBuilders.post("/brands")
            .accept(MediaType.APPLICATION_JSON_VALUE)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(objectMapper.writeValueAsString(request));
    ResultActions resultActions = mockMvc.perform(requestBuilder);
    resultActions
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("@.name", Is.is("brand A")));
  }

  @Test
  public void shouldReturn400HttpStatusForCreateDuplicatedBrand() throws Exception {
    final Brand request = Brand.builder().name("brand A").build();
    final MockHttpServletRequestBuilder requestBuilder =
        MockMvcRequestBuilders.post("/brands")
            .accept(MediaType.APPLICATION_JSON_VALUE)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(objectMapper.writeValueAsString(request));
    mockMvc.perform(requestBuilder);
    ResultActions resultActions = mockMvc.perform(requestBuilder);
    resultActions
        .andExpect(status().isBadRequest())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
  }

  @Test
  public void shouldReturnBrandByIdAnd200HttpStatus() throws Exception {
    final MockHttpServletRequestBuilder requestBuilder =
        MockMvcRequestBuilders.get("/brands/1")
            .accept(MediaType.APPLICATION_JSON_VALUE)
            .contentType(MediaType.APPLICATION_JSON_VALUE);
    ResultActions resultActions = mockMvc.perform(requestBuilder);
    resultActions
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("@.name", Is.is("Nike")));
  }

  @Test
  public void shouldReturn404HttpStatusWhenBrandIsNotFoundById() throws Exception {
    final MockHttpServletRequestBuilder requestBuilder =
        MockMvcRequestBuilders.get("/brands/1000")
            .accept(MediaType.APPLICATION_JSON_VALUE)
            .contentType(MediaType.APPLICATION_JSON_VALUE);
    ResultActions resultActions = mockMvc.perform(requestBuilder);
    resultActions
        .andExpect(status().isNotFound())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
  }

  @Test
  public void shouldDeleteBrandByIdAndReturn204HttpStatus() throws Exception {
    long quantityBeforeDelete = quantityRepository.count();
    final MockHttpServletRequestBuilder requestBuilder =
        MockMvcRequestBuilders.delete("/brands/1")
            .accept(MediaType.APPLICATION_JSON_VALUE)
            .contentType(MediaType.APPLICATION_JSON_VALUE);
    ResultActions resultActions = mockMvc.perform(requestBuilder);
    resultActions.andExpect(status().isNoContent());
    long quantityAfterDelete = quantityRepository.count();
    Assert.assertEquals(quantityBeforeDelete - 2, quantityAfterDelete);
  }

  @Test
  public void shouldReturn404HttpStatusWhenDeletedBrandIsNotFoundById() throws Exception {
    final MockHttpServletRequestBuilder requestBuilder =
        MockMvcRequestBuilders.delete("/brands/1000")
            .accept(MediaType.APPLICATION_JSON_VALUE)
            .contentType(MediaType.APPLICATION_JSON_VALUE);
    ResultActions resultActions = mockMvc.perform(requestBuilder);
    resultActions
        .andExpect(status().isNotFound())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
  }

  @Test
  public void shouldUpdateBrandNameAndReturn200HttpStatus() throws Exception {
    final Brand request = Brand.builder().name("brand A").build();
    final MockHttpServletRequestBuilder requestBuilder =
        MockMvcRequestBuilders.put("/brands/1")
            .accept(MediaType.APPLICATION_JSON_VALUE)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(objectMapper.writeValueAsString(request));
    ResultActions resultActions = mockMvc.perform(requestBuilder);
    resultActions
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("@.name", Is.is("brand A")));
  }

  @Test
  public void shouldReturn404HttpStatusWhenUpdatedBrandIsNotFoundById() throws Exception {
    final Brand request = Brand.builder().name("brand A").build();
    final MockHttpServletRequestBuilder requestBuilder =
        MockMvcRequestBuilders.put("/brands/1000")
            .accept(MediaType.APPLICATION_JSON_VALUE)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(objectMapper.writeValueAsString(request));
    ResultActions resultActions = mockMvc.perform(requestBuilder);
    resultActions
        .andExpect(status().isNotFound())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE));
  }

  @Test
  public void shouldGenerateSumReport() throws Exception {
    final MockHttpServletRequestBuilder requestBuilder =
        MockMvcRequestBuilders.get("/brands/reports/sum")
            .queryParam("sort", "name,desc")
            .queryParam("page", "0")
            .queryParam("size", "5")
            .accept(MediaType.APPLICATION_JSON_VALUE)
            .contentType(MediaType.APPLICATION_JSON_VALUE);
    ResultActions resultActions = mockMvc.perform(requestBuilder);

    resultActions
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(jsonPath("@.content[0].id", Is.is(3)))
        .andExpect(jsonPath("@.content[0].name", Is.is("Puma")))
        .andExpect(jsonPath("@.content[0].quantity", Is.is(0)))
        .andExpect(jsonPath("@.content[1].id", Is.is(1)))
        .andExpect(jsonPath("@.content[1].name", Is.is("Nike")))
        .andExpect(jsonPath("@.content[1].quantity", Is.is(50)))
        .andExpect(jsonPath("@.content[2].id", Is.is(2)))
        .andExpect(jsonPath("@.content[2].name", Is.is("Adidas")))
        .andExpect(jsonPath("@.content[2].quantity", Is.is(40)));
  }
}
