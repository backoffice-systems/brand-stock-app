INSERT INTO brand (brand_id, name)
    VALUES (1, 'Nike'),
           (2, 'Adidas'),
           (3, 'Puma');

INSERT INTO quantity (id, quantity, time_received, brand_id)
    VALUES (1, 20, now(), 1),
           (2, 30, now(), 1),
           (3, 40, now(), 2);